export const environment = {
  production: true,
  projectName: 'ProjectX',
  api: 'http://localhost:8000/api/'
  // api: 'http://project-x-api.project-release.info/api/'
};
