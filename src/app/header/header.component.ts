import { Component, OnInit } from '@angular/core';
import {UserService} from '../common/services/user.service';
import {User} from '../common/models/user.mode';
import {AuthService} from '../common/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public aboutUser: User;
  constructor(private userService: UserService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.userService.aboutUser.subscribe((data) => {
      this.aboutUser = data;
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['']);
  }

}
