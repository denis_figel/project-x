export interface ActionModel {
  id: number;
  name: string;
  title: string;
  url_to: string;
  message: string;
  image: string;
  desktop_placement: number;
  mobile_placement: number;
  delay: number;
  time: number;
  repeat: boolean;
  close: boolean;
  new_tab: boolean;
  template_id: number;
  round: boolean;
  title_color: string;
  background_color: string;
  message_color: string;
  popups: Array<ActionPopupModel>;
}

export interface ActionPopupModel {
  created_at: Date;
  event_id: number;
  id: number;
  image: string;
  message: string
  title: string
  updated_at: Date;
  url_to: string;
}
