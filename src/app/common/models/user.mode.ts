export interface User {
  id: number;
  email: string;
  email_verified_at: string;
  name: string;
  phoneNumber: string;
  created_at: Date;
  updated_at: string;
}
