import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let copiedReq = req.clone();
    if (this.authService.isAuth) {
      copiedReq = req.clone({headers: req.headers.set('Authorization', this.authService.getToken())});
    }
    return next.handle(copiedReq).pipe(catchError(err => {
      if (err.status === 401) {
        // auto logout if 401 response returned from api
        this.authService.logout();
        // location.reload(true);
      }

      // const error = err.error.message || err.statusText;
      return throwError(err);
    }));
  }
}
