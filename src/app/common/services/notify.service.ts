import {Injectable} from '@angular/core';
import {NotifierService} from 'angular-notifier';

@Injectable()
export class NotifyService {
  constructor(private notifier: NotifierService) {}

  /*
  * type: success, error, warning, info, default
  * */
  notify(type: string, message: string, id?: string) {
    this.notifier.notify( type, message, id );
  }

  parseErrors(errors) {
    if (errors.errors) {
      for (const key in errors.errors) {
        errors.errors[key].forEach((v, k) => {
          this.notifier.notify('error', `${v}`);
        });
      }
    } else {
      this.notifier.notify('error', `${errors.error}`);
    }
    console.log('Error', errors);
  }

  hide(id: string) {
    this.notifier.hide(id);
  }

  hideNewest() {
    this.notifier.hideNewest();
  }

  hideOldest() {
    this.notifier.hideOldest();
  }

  hideAll() {
    this.notifier.hideAll();
  }
}
