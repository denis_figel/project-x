import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}
  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (route.routeConfig.path === '') {
      if (this.authService.getAuth()) {
        this.router.navigate(['/dashboard/adverts']);
        return false;
      } else {
        return true;
      }
    } else {
      if (!this.authService.getAuth()) {
        this.router.navigate(['']);
        return false;
      } else {
        return true;
      }
    }
  }
}
