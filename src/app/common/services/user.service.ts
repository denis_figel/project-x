import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class UserService {
  aboutUser = new EventEmitter<any>();
  constructor(private http: HttpClient) {}

  getAuthUserInfo() {
    return this.http.get(environment.api + 'user/get').pipe(
      map((res: Response) => {
        this.aboutUser.emit(res['user']);
        return res['user'];
      })
    );
    // return this.aboutUser;
  }

  updateUserInfo(data) {
    return this.http.post(environment.api + 'user/edit', data);
  }
}
