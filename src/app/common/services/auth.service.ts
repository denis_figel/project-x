import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthService {
  isAuth = false;

  constructor(private http: HttpClient) {}

  checkUserEmail(email: string) {
    return this.http.post(environment.api + 'auth/checkEmail', {email}).pipe(
      map(res => res)
    );
  }

  resetUserPassword(email: string) {
    return this.http.post(environment.api + 'auth/recover', {email});
  }

  registerUser(data: any[]) {
    return this.http.post(environment.api + 'auth/register', data);
  }

  login(data: any[]) {
    // this.isAuth = true;

    // return this.isAuth;
    return this.http.post(environment.api + 'auth/login', data);
  }

  logout() {
    this.isAuth = false;
    localStorage.removeItem('token');
    return true;
  }

  setAuth(token) {
    localStorage.setItem('token', token);
    this.isAuth = true;
  }

  getAuth() {
    if (localStorage.getItem('token')) {
      this.isAuth = true;
    }
    return this.isAuth;
  }

  getToken() {
    return 'Bearer ' + localStorage.getItem('token');
  }
}
