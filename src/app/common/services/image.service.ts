import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ImageService {
  constructor(private http: HttpClient) {}

  uploadFile(file) {
    return this.http.post(environment.api + 'image/store', file);
  }
}
