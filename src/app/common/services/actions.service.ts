import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {ActionModel} from '../models/action.model';

@Injectable()
export class ActionsService {
  constructor(private http: HttpClient) {}

  getAllData() {
    return this.http.get(environment.api + 'events').pipe(
      map(res => res['data'])
    );
  }

  getById(id: number) {
    return this.http.get(environment.api + 'events/' + id);
  }

  add(data) {
    return this.http.post(environment.api + 'events', data);
  }

  update(id: number, data: ActionModel) {
    return this.http.put(environment.api + 'events/' + id, data);
  }

  delete(id) {
    return this.http.delete(environment.api + 'events/' + id);
  }

  getTemplates() {
    return this.http.get(environment.api + 'templates/get');
  }
}
