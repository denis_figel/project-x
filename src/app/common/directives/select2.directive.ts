import {Directive, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';

@Directive({
  selector: '[appSelect2]'
})
export class Select2Directive implements OnInit {
  @Output() ngModelChange = new EventEmitter<string>();
  constructor(private elementRef: ElementRef) {}
  ngOnInit() {
    (<any>$(this.elementRef.nativeElement)).select2().on('select2:select', (e) => {
      this.ngModelChange.emit(e.currentTarget.value);
    });
  }
}
