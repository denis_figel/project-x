import {Directive, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';

interface JQuery {
   iCheck(checkboxClass?: any, radioClass?: string, increaseArea?: string): void;
}

@Directive({
  selector: '[appICheck]'
})
export class ICheckDirective implements OnInit {
  @Output() ngModelChange = new EventEmitter<boolean>();
  @Output('changeValue') onCheckChange = new EventEmitter<boolean>();
  @Input('ngModel') checked: boolean;

  constructor(private elementRef: ElementRef) {}
  ngOnInit() {
    (<any>$(this.elementRef.nativeElement)).iCheck(this.checked ? 'check' : 'uncheck');

    (<any>$(this.elementRef.nativeElement)).iCheck(
      {
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
      }
    )
    .on('ifToggled', (e: any) => {
      this.ngModelChange.emit(e.currentTarget.checked);
      this.onCheckChange.emit(e.currentTarget.checked);
    });
  }
}
