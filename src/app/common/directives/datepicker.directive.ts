import {Directive, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';

@Directive({
  selector: '[appDatepicker]'
})
export class DatepickerDirective implements OnInit{
  @Output() ngModelChange = new EventEmitter<string>();
  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    const endDate = new Date();
    (<any>$(this.elementRef.nativeElement)).datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      endDate: endDate
    }).on('changeDate', (e) => {
      this.ngModelChange.emit(e.currentTarget.value);
    });
  }
}
