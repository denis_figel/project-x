import {Directive, ElementRef, HostListener, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appToggleNavigation]'
})
export class ToggleNavigationDirective implements OnInit {
  isActive = false;

  constructor(private renderer: Renderer2, private elementRef: ElementRef) {}
  ngOnInit() {}

  @HostListener('click') click() {
    this.isActive = !this.isActive;
    if (this.isActive) {
      this.renderer.addClass(document.body, 'sidebar-collapse');
      this.renderer.addClass(document.body, 'sidebar-open');
    } else {
      this.renderer.removeClass(document.body, 'sidebar-collapse');
      this.renderer.removeClass(document.body, 'sidebar-open');
    }
  }
}
