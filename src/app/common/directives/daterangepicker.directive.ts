import {Directive, ElementRef, OnInit} from '@angular/core';
import * as moment from 'moment';

@Directive({
  selector: '[appDaterange]'
})
export class DaterangepickerDirective implements OnInit {
  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    console.log(moment().subtract(6, 'months').format('DD/MM/YYYY'));

    (<any>$(this.elementRef.nativeElement)).daterangepicker({
      maxDate: new Date(),
      locale: {
        format: 'DD/MM/YYYY'
      },
      // maxSpan: {
      //   'days': 7
      // },
      // startDate: moment().subtract(1, 'months').format('DD/MM/YYYY'),
      // endDate: new Date(),
      autoApply: true
    }).on('apply.daterangepicker', (ev, picker) => {
      console.log('apply', ev);
    }).on('hide.daterangepicker', (ev, picker) => {
      console.log('hideCalendar', picker);
    });
  }
}
