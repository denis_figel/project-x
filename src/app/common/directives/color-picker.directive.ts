import {Directive, ElementRef, EventEmitter, OnInit, Output, Input} from '@angular/core';

@Directive({
  selector: '[appColorPicker]'
})
export class ColorPickerDirective implements OnInit {
  @Output() ngModelChange = new EventEmitter<string>();
  @Input('appColorPicker') color;
  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    (<any>$(this.elementRef.nativeElement)).colorpicker().on('changeColor', (e) => {
      this.ngModelChange.emit(e.currentTarget.value);
    });

    if (this.color) {
      (<any>$(this.elementRef.nativeElement)).colorpicker().on('showPicker', (e) => {
        (<any>$(this.elementRef.nativeElement)).colorpicker('setValue', this.color);
        this.ngModelChange.emit(e.currentTarget.value);
      });
    }
  }
}
