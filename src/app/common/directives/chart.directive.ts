import {Directive, ElementRef, OnInit} from '@angular/core';

declare let Chart: any;

@Directive({
  selector: '[appChart]'
})
export class ChartDirective implements OnInit {

  areaChartData = {
    labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label               : 'Views',
        fillColor           : '#19C0EC',
        strokeColor         : '#19C0EC',
        pointColor          : '#19C0EC',
        pointStrokeColor    : '#19C0EC',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(220,220,220,1)',
        data                : [65, 59, 80, 81, 56, 55, 10]
      },
      {
        label               : 'Clicks',
        fillColor           : '#1BA45F',
        strokeColor         : '#1BA45F',
        pointColor          : '#1BA45F',
        pointStrokeColor    : '#1BA45F',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : [18, 38, 42, 21, 46, 17, 20]
      },
      {
        label               : 'Closes',
        fillColor           : '#F29931',
        strokeColor         : '#F29931',
        pointColor          : '#F29931',
        pointStrokeColor    : '#F29931',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : [28, 48, 40, 19, 86, 27, 90]
      }
    ]
  };

  areaChartOptions = {
    showScale               : true,
    scaleShowGridLines      : true,
    scaleGridLineColor      : 'rgba(0,0,0,.05)',
    scaleGridLineWidth      : 1,
    scaleShowHorizontalLines: true,
    scaleShowVerticalLines  : true,
    bezierCurve             : true,
    bezierCurveTension      : 0.3,
    pointDot                : true,
    pointDotRadius          : 4,
    pointDotStrokeWidth     : 1,
    pointHitDetectionRadius : 20,
    datasetStroke           : true,
    datasetStrokeWidth      : 2,
    datasetFill             : true,
    legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
    maintainAspectRatio     : true,
    responsive              : true
  };
  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
    const lineChartCanvas          = (<any>$(this.elementRef.nativeElement)).get(0).getContext('2d');
    const lineChart                = new Chart(lineChartCanvas);
    const lineChartOptions         = this.areaChartOptions;
    lineChartOptions.datasetFill   = false;
    lineChart.Line(this.areaChartData, lineChartOptions);
  }
}
