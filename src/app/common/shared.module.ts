import {NgModule} from '@angular/core';
import {ICheckDirective} from './directives/icheck.directive';
import {Select2Directive} from './directives/select2.directive';
import {ColorPickerDirective} from './directives/color-picker.directive';
// import {FileSelectDirective} from 'ng2-file-upload';

@NgModule({
  declarations: [
    ICheckDirective,
    Select2Directive,
    ColorPickerDirective,
    // FileSelectDirective
  ],
  exports: [
    ICheckDirective,
    Select2Directive,
    ColorPickerDirective,
    // FileSelectDirective
  ]
})
export class SharedModule {}
