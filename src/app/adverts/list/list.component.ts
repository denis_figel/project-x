import { Component, OnInit } from '@angular/core';
import {ActionsService} from '../../common/services/actions.service';
import {NotifyService} from '../../common/services/notify.service';
import {ConfirmationService} from '@jaspero/ng2-confirmations';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  // settings: any = {
  //   overlay: true,
  //   overlayClickToClose: false,
  //   showCloseButton: true,
  //   confirmText: 'Yes',
  //   declineText: 'No'
  // };
  allEvents = [];
  copyCode = '';
  constructor(
    private actionService: ActionsService,
    private notifier: NotifyService,
    private _confirmation: ConfirmationService
  ) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.actionService.getAllData()
      .subscribe(
        (res) => {
          this.allEvents = res;
        },
        (error) => {
          this.notifier.notify('error', 'Please check your credentials and try again.');
          console.log('error', error);
        }
      );
  }

  onDelete(id: number) {
    this._confirmation.create('Delete this action', 'Are you sure to delete this action?')
      .subscribe((ans: any) => {
        if (ans.resolved === true) {
          this.actionService.delete(id)
            .subscribe(
              (res) => {
                this.getAll();
              },
              (error) => {
                this.notifier.notify('error', 'Please check your credentials and try again.');
                console.log('error', error);
              }
            );
        }
      });
  }

  onCopy(index: number) {
    this.copyCode = this.allEvents[index].script;
  }
}
