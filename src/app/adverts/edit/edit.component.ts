import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ActionsService} from '../../common/services/actions.service';
import {NotifyService} from '../../common/services/notify.service';
import {NgForm} from '@angular/forms';
import {ActionModel} from '../../common/models/action.model';
import {ImageService} from '../../common/services/image.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  formData: ActionModel;
  fileUpload: File = null;
  uploadedFile = '';
  templates: Array<any> = [];

  constructor(private notify: NotifyService,
              private route: ActivatedRoute,
              private router: Router,
              private actionService: ActionsService,
              private imageService: ImageService
  ) { }

  ngOnInit() {
    if (!this.route.snapshot.params.id) {
      this.router.navigate(['adverts']);
    } else {
      this.getAbout(this.route.snapshot.params.id);
      this.getAllTemplates();
    }
  }

  getAllTemplates() {
    this.actionService.getTemplates()
      .subscribe(
        (res: any) => {
          this.templates = res.templates;
        },
        (error) => {
          this.notify.parseErrors(error.error);
        }
      );
  }

  onFileSelected(event) {
    this.fileUpload = <File>event.target.files[0];
    const fd = new FormData();
    fd.append('image', this.fileUpload, this.fileUpload.name);
    this.imageService.uploadFile(fd)
      .subscribe(
        (res: any) => {
          this.uploadedFile = res.image;

          // this.notify.notify('success', 'Image was successfully uplaoded.');
        },
        (error) => {
          this.notify.parseErrors(error.error);
        }
      );
  }

  getAbout(id: number) {
    this.actionService.getById(id)
      .subscribe(
        (res: ActionModel) => {
          this.formData = res;
          this.uploadedFile = this.formData.image;

          console.log(this.formData);
        },
        (error) => {
          this.notify.parseErrors(error.error);
        }
      );
  }

  update(form: NgForm) {
    form.value.image = this.uploadedFile;
    this.actionService.update(this.formData.id, form.value)
      .subscribe(
        (res: ActionModel) => {
          this.notify.notify('success', 'Action was successfully updated.');
        },
        (error) => {
          this.notify.parseErrors(error.error);
        }
      );
  }

}
