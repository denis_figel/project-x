import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotifyService } from '../../common/services/notify.service';
import { ActionsService } from '../../common/services/actions.service';
import { AuthService } from '../../common/services/auth.service';
import { ImageService } from '../../common/services/image.service';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  round = false;
  repeat = false;
  close = false;
  new_tab = false;
  desktop_placement = '1';
  mobile_placement = '1';
  template_id = '';
  title_color = '';
  background_color = '';
  message_color = '';
  fileUpload: File = null;
  uploadedFile = '';
  templates: Array<any> = [];
  popupsArray: Array<any> = [];

  constructor(private notify: NotifyService,
              private actionService: ActionsService,
              private authService: AuthService,
              private imageService: ImageService
  ) { }

  ngOnInit() {
    this.getAllTemplates();
  }

  getAllTemplates() {
    this.actionService.getTemplates()
      .subscribe(
        (res: any) => {
          this.templates = res.templates;
          this.template_id = this.templates[0].id;

        },
        (error) => {
          this.notify.parseErrors(error.error);
        }
      );
  }

  onFileSelected(event) {
    this.fileUpload = <File>event.target.files[0];
    const fd = new FormData();
    fd.append('image', this.fileUpload, this.fileUpload.name);
    this.imageService.uploadFile(fd)
      .subscribe(
        (res: any) => {
          this.uploadedFile = res.image;

          // this.notify.notify('success', 'Image was successfully uplaoded.');
        },
        (error) => {
          this.notify.parseErrors(error.error);
        }
      );
  }

  onAddPopup(form: NgForm) {
    form.value.image = this.uploadedFile;
    this.popupsArray.push(form.value);

    form.reset();
    this.uploadedFile = '';

    console.log(this.popupsArray);
  }

  onDeletePopup(index: number) {
    this.popupsArray.splice(index, 1);
  }

  add(form: NgForm) {
    if(this.popupsArray.length <= 0) {
      this.notify.notify('error', 'Please create at least one popup.');
      return false;
    }
    form.value.image = this.uploadedFile;
    form.value.popups = this.popupsArray;

    this.actionService.add(form.value)
      .subscribe(
        (res) => {
          console.log(res);
          this.notify.notify('success', 'Action was successfully save.');
          form.reset();
          this.uploadedFile = '';
          this.popupsArray = [];
        },
        (error) => {
          this.notify.parseErrors(error.error);
        }
      );
  }

}
