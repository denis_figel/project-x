import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './home/dashboard.component';
import { AdvertsComponent } from '../adverts/adverts.component';
import { StatisticsComponent } from '../statistics/statistics.component';
import { BillingComponent } from '../billing/billing.component';
import { MailComponent } from '../mail/mail.component';
import { ProfileComponent } from '../profile/profile.component';
import { ListComponent } from '../adverts/list/list.component';
import { AddComponent } from '../adverts/add/add.component';
import { EditComponent } from '../adverts/edit/edit.component';

const routes: Routes = [
  {path: '', component: DashboardComponent, children: [
      {path: 'adverts', component: AdvertsComponent, children: [
        {path: '', component: ListComponent},
        {path: 'add', component: AddComponent},
        {path: 'edit/:id', component: EditComponent}
      ]},
      {path: 'statistics', component: StatisticsComponent},
      {path: 'billing', component: BillingComponent},
      {path: 'mail', component: MailComponent},
      {path: 'profile', component: ProfileComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutesModule {}
