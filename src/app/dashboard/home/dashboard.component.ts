import {Component, OnInit, Renderer2, ViewEncapsulation} from '@angular/core';
import {User} from '../../common/models/user.mode';
import {UserService} from '../../common/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  constructor(private renderer: Renderer2, private userService: UserService) { }

  ngOnInit() {
    // this.renderer.addClass(document.body, 'hold-transition');
    this.renderer.addClass(document.body, 'skin-blue');
    this.renderer.addClass(document.body, 'sidebar-mini');
    // this.renderer.addClass(document.body, 'sidebar-collapse');

    this.userService.getAuthUserInfo()
      .subscribe((data: User) => {
        // this.userService.aboutUser.emit(data);
        // this.userService.userData = data;

        // console.log('data', data);
      });
  }

}
