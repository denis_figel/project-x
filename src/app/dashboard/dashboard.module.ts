import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DashboardRoutesModule } from './dashboard.routes.module';
import { DashboardComponent } from './home/dashboard.component';
import { HeaderComponent } from '../header/header.component';
import { NavigationComponent } from '../navigation/navigation.component';
import { AdvertsComponent } from '../adverts/adverts.component';
import { FooterComponent } from '../footer/footer.component';
import { ToggleNavigationDirective } from '../common/directives/toggle.navigation.directive';
import { SetActiveBlockDirective } from '../header/directives/set.active.block.directive';
import { MenuDirective } from '../navigation/directives/menu.directive';
import { StatisticsComponent } from '../statistics/statistics.component';
import { BillingComponent } from '../billing/billing.component';
import { MailComponent } from '../mail/mail.component';
import { UserService } from '../common/services/user.service';
import { ProfileComponent } from '../profile/profile.component';
import { ActionsService } from '../common/services/actions.service';
import { ListComponent } from '../adverts/list/list.component';
import { AddComponent } from '../adverts/add/add.component';
import { EditComponent } from '../adverts/edit/edit.component';
import { SharedModule } from '../common/shared.module';
import { ImageService } from '../common/services/image.service';
import { JasperoConfirmationsModule } from '@jaspero/ng2-confirmations';
import { DatepickerDirective } from '../common/directives/datepicker.directive';
import {ChartDirective} from '../common/directives/chart.directive';
import {DaterangepickerDirective} from '../common/directives/daterangepicker.directive';

@NgModule({
  declarations: [
    DashboardComponent,
    HeaderComponent,
    NavigationComponent,
    AdvertsComponent,
    ListComponent,
    AddComponent,
    EditComponent,
    FooterComponent,
    ToggleNavigationDirective,
    SetActiveBlockDirective,
    MenuDirective,
    StatisticsComponent,
    BillingComponent,
    MailComponent,
    ProfileComponent,
    DatepickerDirective,
    ChartDirective,
    DaterangepickerDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DashboardRoutesModule,
    SharedModule,
    JasperoConfirmationsModule
  ],
  providers: [UserService, ActionsService, ImageService],
})

export class DashboardModule {}
