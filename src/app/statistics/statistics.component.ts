import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { ActionsService } from '../common/services/actions.service';
import {NotifyService} from '../common/services/notify.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class StatisticsComponent implements OnInit {

  eventDate = '';
  actionId = 0;
  allEvents = [];
  constructor(private actionService: ActionsService,
              private notify: NotifyService) { }

  ngOnInit() {
    this.actionService.getAllData()
      .subscribe(
        (res) => {
          this.allEvents = res;
          this.actionId = (res[0]) ? res[0].id : 0;
          console.log(res);
        },
        (error) => {
          this.notify.notify('error', 'Please check your credentials and try again.');
          console.log('error', error);
        }
      );
  }

}
