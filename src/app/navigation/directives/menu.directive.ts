import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appMenu]'
})
export class MenuDirective {
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  @HostListener('click') click() {
    $('ul.sidebar-menu').find('li').removeClass('active');

    this.renderer.addClass(this.elementRef.nativeElement, 'active');
    // if ($(this.elementRef.nativeElement).hasClass('menu-open')) {
    //   this.renderer.removeClass(this.elementRef.nativeElement, 'menu-open');
    //   this.renderer.removeClass(this.elementRef.nativeElement, 'active');
    //   $(this.elementRef.nativeElement).find('ul.treeview-menu').slideUp('fast');
    // } else {
    //   this.renderer.addClass(this.elementRef.nativeElement, 'menu-open');
    //   this.renderer.addClass(this.elementRef.nativeElement, 'active');
    //   $(this.elementRef.nativeElement).find('ul.treeview-menu').slideDown('fast');
    // }
  }
}
