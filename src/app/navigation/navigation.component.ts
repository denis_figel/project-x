import { Component, OnInit } from '@angular/core';
import { UserService } from '../common/services/user.service';
import {User} from '../common/models/user.mode';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  public aboutUser: User;
  constructor(private userService: UserService) { }

  ngOnInit() {
    // this.userService.getAuthUserInfo()
    //   .subscribe((data: User) => {
    //     this.userService.aboutUser.emit(data);
    //     this.aboutUser = data;
    //   });

    this.userService.aboutUser.subscribe((data) => {
      this.aboutUser = data;
    });
  }

}
