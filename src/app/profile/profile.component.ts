import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../common/services/user.service';
import {User} from '../common/models/user.mode';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NotifyService} from '../common/services/notify.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  aboutUser: User;
  userForm: FormGroup;
  constructor(private userService: UserService, private notifier: NotifyService) {
    this.getAboutUser();
  }

  ngOnInit() {}

  getAboutUser() {
    this.userService.getAuthUserInfo()
      .subscribe((data: User) => {
        this.aboutUser = data;
        // console.log(this.aboutUser);

        this.createForm();
      });
  }

  createForm() {
    this.userForm = new FormGroup({
      'name': new FormControl(this.aboutUser.name, [Validators.required]),
      'email': new FormControl(this.aboutUser.email, [Validators.required, Validators.email]),
      'phoneNumber': new FormControl(this.aboutUser.phoneNumber),
      'password': new FormControl(''),
      'password_confirmation': new FormControl('', [this.comparePasswords.bind(this)]),
      'old_password': new FormControl('')
    });
  }

  comparePasswords(control: FormControl): {[s: string]: boolean} {
    if(this.userForm) {
      if(this.userForm.get('password').value !== control.value) {
        return {'passwordsDoNotMatch': true};
      }
      return null;
    }
  }

  updateUser() {
    this.userService.updateUserInfo(this.userForm.value)
      .subscribe(
        (res) => {
          // console.log(res);
          this.userService.aboutUser.emit(res);
          this.notifier.notify('success', 'User profile was successfully save.');

          // this.signUpForm.reset();
        },
        (error) => {
          this.notifier.parseErrors(error.error);
        }
      );
    // console.log(this.userForm.value);
  }

  ngOnDestroy() {
    // this.userService.aboutUser.unsubscribe();
  }

}
