import { Component, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../common/services/auth.service';
import { Router } from '@angular/router';
import { NotifyService } from '../../common/services/notify.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  rememberMe = false;

  constructor(private renderer: Renderer2, private authService: AuthService, private router: Router, private notifier: NotifyService) {
    this.renderer.addClass(document.body, 'hold-transition');
    this.renderer.addClass(document.body, 'login-page');
  }

  ngOnInit() {}

  onLogin(form: NgForm): void {
    this.authService.login(form.value)
      .subscribe(
        (response) => {
           this.authService.setAuth(response['token']);
           this.router.navigate(['/dashboard/adverts']);
           // console.log('response', response);
        },
        (error) => {
          this.notifier.notify('error', 'Please check your credentials and try again.');
          console.log('error', error);
        }
      );
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'hold-transition');
    this.renderer.removeClass(document.body, 'login-page');
  }
}
