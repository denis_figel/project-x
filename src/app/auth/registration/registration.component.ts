import { Component, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../common/services/auth.service';
import { NotifyService } from '../../common/services/notify.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit, OnDestroy {
  signUpForm: FormGroup;
  accept = false;

  constructor(private renderer: Renderer2, private authService: AuthService, private notifier: NotifyService) { }

  ngOnInit() {
    this.renderer.addClass(document.body, 'hold-transition');
    this.renderer.addClass(document.body, 'register-page');

    this.signUpForm = new FormGroup({
      'name': new FormControl(null, [Validators.required]),
      'phoneNumber': new FormControl(null),
      'email': new FormControl(null, [Validators.required, Validators.email], this.checkEmailValidation.bind(this)),
      'password': new FormControl(null, [Validators.required]),
      'rePassword': new FormControl(null, [Validators.required, this.comparePasswords.bind(this)]),
      'accept': new FormControl(this.accept, [Validators.required])
    });
  }

  comparePasswords(control: FormControl): {[s: string]: boolean} {
    if(this.signUpForm) {
      if(this.signUpForm.get('password').value !== control.value) {
        return {'passwordsDoNotMatch': true};
      }
      return null;
    }
  }

  checkEmailValidation(control: FormControl) {
    return new Promise((resolve, reject) => {
      this.authService.checkUserEmail(control.value)
        .subscribe(
          (res) => {
            if (res['email'] !== 'empty') {
              resolve({ emailExists: true });
            } else {
              resolve(null);
            }
          },
          (error) => {
            resolve({ emailExistsError: true });
          }
        );
    });
  }

  onRegister() {
    //console.log(this.signUpForm);

    this.authService.registerUser(this.signUpForm.value)
      .subscribe(
        (res) => {
          // console.log(res);
          this.notifier.notify('success', 'Registration is success. Please use your credentials to login.');

          this.signUpForm.reset();
        },
        (error) => {
          this.notifier.parseErrors(error.error);
        }
      );
  }

  onCheckChange(event: Event) {
    this.signUpForm.patchValue({
      'accept': event
    });
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'hold-transition');
    this.renderer.removeClass(document.body, 'register-page');
  }

}
