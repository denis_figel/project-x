import {Component, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../common/services/auth.service';
import {NotifyService} from '../../common/services/notify.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit, OnDestroy {
  resetForm: FormGroup;

  constructor(private renderer: Renderer2, private authService: AuthService, private notifier: NotifyService) {
    renderer.addClass(document.body, 'hold-transition');
    renderer.addClass(document.body, 'login-page');
  }

  ngOnInit() {
    this.resetForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email], this.checkEmailValidation.bind(this))
    });
  }

  onResetPassword() {
    this.authService.resetUserPassword(this.resetForm.value['email'])
      .subscribe(
        (res) => {
          this.notifier.notify('success', 'Please check your email to reset password.');
          this.resetForm.reset();
        },
        (error) => {
          this.notifier.parseErrors(error.error)
          console.log('Error', error);
        }
      );

    // console.log(this.resetForm);
  }

  checkEmailValidation(control: FormControl) {
    return new Promise((resolve, reject) => {
      this.authService.checkUserEmail(control.value)
        .subscribe(
          (res) => {
            if (res['email'] === 'not_empty') {
              resolve(null);
            } else {
              resolve({ emailExists: true });
            }
          },
          (error) => {
            resolve({ emailExistsError: true });
          }
        );
    });
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'hold-transition');
    this.renderer.removeClass(document.body, 'login-page');
  }

}
