import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.css']
})
export class SocialComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

  onLoginWith(social: string) {
    if(!social) {
      throw Error('No social is provided!');
    }

    console.log(social);
  }

}
